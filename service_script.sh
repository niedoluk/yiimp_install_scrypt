#!/bin/bash
LOG_DIR=/var/log
WEB_DIR=/var/web
STRATUM_DIR=/var/stratum
USR_BIN=/usr/bin
STRATUM_DIR=/var/stratum



cat >> /lib/systemd/system/poolmain.service <<EOF
[Unit]
Description=Pool main service. Debug is in /var/log/debug.log. Next services: poolmain, poolblocks, poolloop. poolyescrypt

[Service]
ExecStart=$WEB_DIR/main.sh
Restart=always
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=pool

[Install]
WantedBy=multi-user.target
EOF
systemctl enable poolmain.service
service poolmain start

cat >> /lib/systemd/system/poolloop.service <<EOF
[Unit]
Description=Pool loop service. Debug is in /var/log/debug.log. Next services: poolmain, poolblocks, poolloop. poolyescrypt

[Service]
ExecStart=$WEB_DIR/loop2.sh
Restart=always
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=pool

[Install]
WantedBy=multi-user.target
EOF
systemctl enable poolloop.service
service poolloop start

cat >> /lib/systemd/system/poolblocks.service <<EOF
[Unit]
Description=Pool blocks service.  Debug is in /var/log/debug.log. Next services: poolmain, poolblocks, poolloop. poolyescrypt

[Service]
ExecStart=$WEB_DIR/blocks.sh
Restart=always
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=pool

[Install]
WantedBy=multi-user.target
EOF
systemctl enable poolblocks.service
service poolblocks start

cat >> /lib/systemd/system/poolyescrypt.service <<EOF
[Unit]
Description=YescryptR16 stratum service.  Debug is in /var/log/debug.log. Next services: poolmain, poolblocks, poolloop. poolyescrypt

[Service]
ExecStart=/bin/bash $STRATUM_DIR/run.sh yescryptR16
Restart=always
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=pool

[Install]
WantedBy=multi-user.target
EOF
systemctl enable poolyescrypt.service
service poolyescrypt start

sudo cat >> /etc/rsyslog.d/pool.conf <<EOF
if \$programname == 'pool' then /var/log/pool.log
if \$programname == 'pool' then ~
EOF
sudo service rsyslog restart


service poolmain status
service poolloop status
service poolblocks status
service poolyescrypt status
