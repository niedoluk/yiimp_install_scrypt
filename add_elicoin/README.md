+ 00 - Please check whether you set your computers ip to /var/web/serverconfig.php
+ 01 - after opening the <yourvpsaddress>/site/panel you will see this page. Click on Wallets
![alt text](01.png "...")
+ 02 - Click CREATE COIN
![alt text](02.png "...")
+ 03 - Fill in Name, Symbol, Algo
![alt text](03.png "...")
+ 04 - Click on Settings, Set Enable and Auto Ready
![alt text](04.png "...")
+ 05 - Click on Daemon, add Process name, your RPC host, RPC port, RPC user and RPC password according to your ~/.elicoin/elicoin.conf
![alt text](05.png "...")
+ 06 - Save coin
